import { GET_ALL_HEROES, GET_SINGLE_HERO } from '../constants/constants'

const initialState = {
  heroes: []
}

const heroes_reducer = (state = initialState, action) => {
  let newState

  switch(action.type) {
    case GET_ALL_HEROES:
      newState = {...state, heroes: action.heroes}
      return newState

    case GET_SINGLE_HERO:
      newState = {...state, heroes: [...state.heroes, action.heroes]}
      return newState
      
    default:
      return state
  }
}

export default heroes_reducer