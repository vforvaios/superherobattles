import { LOADING } from '../constants/constants'

const initialState = {
  isLoading: false
}

const loading_reducer = (state = initialState, action) => {
  let newState

  switch (action.type) {
    case LOADING:
      newState = {...state, isLoading: action.loading}
      return newState

    default:
      return state
  }
}

export default loading_reducer