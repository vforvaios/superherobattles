import { combineReducers } from 'redux'
import heroes_reducer from './heroes_reducer'
import loading_reducer from './loading_reducer'
// import loader_reducer from './loader_reducer'

const rootReducer = combineReducers({
    heroes_reducer,
    loading_reducer
  })

export default rootReducer