import React from 'react'

// packages
import { Provider } from "react-redux"
import store from "./store/store"
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

// components
import Startscreen from './components/Startscreen'
import Allheroes from './components/Allheroes'
import Herodetailsscreen from './components/Herodetailsscreen'
import Loader from './components/Loader'
import Battle from './components/Battle'
import Page404 from './components/Page404'
import ErrorBoundary from './components/ErrorBoundary'
import MusicBackground from './components/MusicBackground'
import './main.scss'

const App = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <ErrorBoundary>
            <Loader />
            <MusicBackground />
            <Switch>
              <Route exact path="/" component={Startscreen}></Route>
              <Route path="/allheroes" component={Allheroes}></Route>
              <Route exact path="/hero/:heroID" component={Herodetailsscreen}></Route>
              <Route exact path="/battle" component={Battle}></Route>
              <Route component={Page404}></Route>
            </Switch>
          </ErrorBoundary>
        </Router>
      </div>
    </Provider>
  );
}

export default App

