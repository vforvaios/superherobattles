import { GET_ALL_HEROES, GET_SINGLE_HERO } from '../constants/constants'
import { handleLoading } from '../actions/loading'
import axios from 'axios'

export const getAllHeroesSuccessful = heroes => {
  return {
    type: GET_ALL_HEROES,
    heroes
  }
}

export const getSingleHeroSuccessful = heroes => {
  return {
    type: GET_SINGLE_HERO,
    heroes
  }
}

export const getAllHeroes = () => {
  return dispatch => {
    dispatch(handleLoading(true))

    axios.get('/api/all.json')
    .then( response => {
      dispatch(getAllHeroesSuccessful(response.data))
    } )
    .then( () => {
      dispatch(handleLoading(false))
    } )
    .catch( error => {
      dispatch(handleLoading(false))
      console.log(error)
    } )
  }
}

export const getSingleHero = heroID => {
  return dispatch => {
    dispatch(handleLoading(true))

    axios.get(`/api/id/${heroID}.json`)
    .then( response => {
      dispatch(getSingleHeroSuccessful(response.data))
    } )
    .then( () => {
      dispatch(handleLoading(false))
    } )
    .catch( error => {
      dispatch(handleLoading(false))
      console.log(error)
    } )
  }
}