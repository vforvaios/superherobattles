import { LOADING } from '../constants/constants'

export const handleLoading = (loading) => {
  return {
    type: LOADING,
    loading
  }
}