import React from 'react'
import { NavLink } from 'react-router-dom';

const Page404 = () => {
  return (
    <div className="content page404">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="page-not-found">
              <h1>Page not Found!</h1>

              <div className="actions">
                <NavLink className="return-link" to="/">
                  Return to homepage!
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Page404