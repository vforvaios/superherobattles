import React from 'react'
import PropTypes from 'prop-types'
import HeroSpecs from './HeroSpecs'

import '../styles/singleheropage.scss'

const SingleHeroPage = ({ h }) => {
  return (
    <div className="row white-text">
      <div className="col-sm-4 col-12">
        <img src={h.images.lg} alt={h.name} className="hero-single-page-image" />
      </div>
      <div className="col-sm-8 col-12">
        <h1 className="white-text mb-5 hero-single-page-name">{ h.name }</h1>

        <HeroSpecs spec={h.biography} labelToShow="BIOGRAPHY" />
        <HeroSpecs spec={h.appearance} labelToShow="APPEARANCE" />
        <HeroSpecs spec={h.powerstats} labelToShow="POWERSTATS" />
        <HeroSpecs spec={h.work} labelToShow="WORK" />
        <HeroSpecs spec={h.connections} labelToShow="CONNECTIONS" />

      </div>
    </div>
  )
}

SingleHeroPage.propTypes = {
  h: PropTypes.object
}

export default SingleHeroPage
