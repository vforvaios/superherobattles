import React from 'react'
import PropTypes from 'prop-types'

const HeroSpecs = ({ spec, labelToShow }) => {
  return (
    <div className={`${labelToShow.toLowerCase()} mb-5`}>
      <h6 className="white-text mb-2">{labelToShow}</h6>
      <div className="bordered">
        {
          Object.keys(spec).map( (sp, index) => {
            return (
              <div className="specs-wrapper dflex" key={index}>
                <div className="flex-item bold hero-desc">{sp.toUpperCase()}</div>
                <div className="flex-item">{spec[sp]}</div>
              </div>
            )
          } )
        }
      </div>
    </div>
  )
}

HeroSpecs.propTypes = {
    spec: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string
    ]),
    labelToShow: PropTypes.string
};

export default HeroSpecs
