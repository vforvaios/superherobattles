import React, { Component } from 'react'
import { connect } from 'react-redux'
import Menu from './Menu'
import Opponent from './Opponent'
import { randomizeArray } from '../variables'
import { getAllHeroes } from '../actions/heroes'
import * as button_sound from '../media/blop.wav'

class Battle extends Component {

  constructor(props, blop_obj) {
    super(props)

    this.state = {
      opponentsChanged: false
    }

    this.blop_obj = blop_obj
  }

  componentDidMount() {
    const { heroes } = this.props

    this.blop_obj = new Audio()
    this.blop_obj.src = button_sound

    if (heroes.length === 0) {
      this.props.getAllHeroes()
    }
  }

  componentDidUpdate(prevState) {
    if (
        prevState.opponentsChanged !== this.state.opponentsChanged &&
        prevState.opponentsChanged
        ) {
      this.setState({ opponentsChanged: false })
    }
  }

  refreshOpponents = () => {
    this.setState({ opponentsChanged: true })
  }

  playButtonSound = () => {
    this.blop_obj.play()
  }

  stopButtonSound = () => {
    this.blop_obj.pause()
    this.blop_obj.currentTime = 0.0
  }

  render() {
    const { heroes } = this.props
    const randHeroes = randomizeArray(heroes)

    return (
      <div className="content battle-page">
        <Menu />
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div className="battle-wrapper">
                <div className="row justify-content-between align-items-center">
                  <div className="col-sm-5 col-12"><Opponent hero={randHeroes[0]} /></div>
                  <div className="col-sm-2 col-12">
                    <img src="/images/vs.png" className="vs-icon" alt="VS Icon" />
                    <button
                      className="navlink"
                      onClick={this.refreshOpponents}
                      onMouseOver={this.playButtonSound}
                      onMouseOut={this.stopButtonSound}>REFRESH BATTLE!</button>
                  </div>
                  <div className="col-sm-5 col-12"><Opponent hero={randHeroes[1]} /></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    heroes: state.heroes_reducer.heroes
  }
}

export default connect(mapStateToProps, { getAllHeroes })(Battle)
