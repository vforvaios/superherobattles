import React, { Component } from 'react'
import { connect } from 'react-redux'

import HeroSingleItem from './HeroSingleItem'
import Menu from './Menu'
import { getAllHeroes } from '../actions/heroes'

class Allheroes extends Component {

  componentDidMount() {
    this.props.getAllHeroes()
  }

  render() {
    const { heroes } = this.props

    return (
      <div className="content allheroes">
        <Menu />

        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <h1 className="text-center m-4 white-text page-title">HEROES LIST</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row">
                {
                  heroes.map( hero => {
                    return <HeroSingleItem key={hero.id} hero={hero} />
                  } )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    heroes: state.heroes_reducer.heroes
  }
}

export default connect(mapStateToProps, { getAllHeroes })(Allheroes)
