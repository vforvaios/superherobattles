import React, { useState, useEffect } from 'react'
import { bg_music } from '../variables'


const MusicBackground = () => {
  const [playMusic, handlePlayMusic] = useState(true)
  

  useEffect(() => {

    if (playMusic) {
      bg_music.play()
    } else {
      bg_music.pause()
      bg_music.currentTime = 0.0
    }

  });

  const playClass = (
    playMusic ? '' : 'stopped'
  )

  return (
    <div className={`sound-player ${playClass}`}>
      <i className="icon-volume-up" onClick={ () => handlePlayMusic(!playMusic) } />
    </div>
  )
}

export default MusicBackground