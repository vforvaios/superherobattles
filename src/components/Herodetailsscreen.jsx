import React, { Component } from 'react'
import { connect } from 'react-redux'
import Menu from './Menu'
import SingleHeroPage from './SingleHeroPage'
import PropTypes from 'prop-types'

import { getSingleHero } from '../actions/heroes'

class Herodetailsscreen extends Component {

  componentDidMount() {
    const heroID = this.props.match.params.heroID
    const { heroes } = this.props

    const isHeroInState = heroes.filter( hero => hero.id === parseInt(heroID) )

    if (isHeroInState.length === 0) {
      this.props.getSingleHero(heroID)
    }
  }

  render() {
    const heroID = this.props.match.params.heroID
    
    const { heroes } = this.props

    return (
      <div className="content single-page-hero">
        <Menu />
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div className="single-hero-wrapper">

                {
                  heroes.filter( hero => hero.id === parseInt(heroID) )
                        .map( h => {
                          return (
                            <SingleHeroPage h={h} key={h.id} />
                          )
                        } )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    heroes: state.heroes_reducer.heroes
  }
}

Herodetailsscreen.propTypes = {
  heroID: PropTypes.number
}

export default connect(mapStateToProps, { getSingleHero })(Herodetailsscreen)
