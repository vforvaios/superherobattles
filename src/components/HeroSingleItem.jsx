import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom';

const HeroSingle = ({ hero }) => {
  return (
    <div key={hero.id} className="col-sm-4 col-6 hero-single-item mb-4">
      <NavLink to={`/hero/${hero.id}`} className="hero-navlink">
        <div className="img-wrapper">
          <img src={hero.images.md} alt={hero.name} />
        </div>
        <div className="lower-hero-info">
          <h4 className="hero-name mt-2 mb-3">
            {hero.name}
          </h4>
          <div className="powerstats">
            {
              Object.keys(hero.powerstats).map( p => {
                return (
                  <div className="powerstat" key={p}>
                    <strong>{`${p}:`}</strong>
                    <span>{hero.powerstats[p]}</span>
                  </div>
                )
              } )
            }
          </div>
        </div>
      </NavLink>
    </div>
  )
}

HeroSingle.propTypes = {
  hero: PropTypes.object
}

export default HeroSingle
