import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom';
import * as button_sound from '../media/blop.wav'

class Startscreen extends Component {
  constructor(props, blop_obj) {
    super(props)
    this.blop_obj = blop_obj
  }

  componentDidMount() {
    this.blop_obj = new Audio()
    this.blop_obj.src = button_sound
  }

  playButtonSound = () => {
    this.blop_obj.play()
  }

  stopButtonSound = () => {
    this.blop_obj.pause()
    this.blop_obj.currentTime = 0.0
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-md-6 offset-md-3 col-8 offset-2">
                <div className="startscreen-actions">
                  <NavLink
                    className="navlink"
                    to="/allheroes"
                    onMouseOver={this.playButtonSound}
                    onMouseOut={this.stopButtonSound}>CHECK ALL HEROES</NavLink>
                  <NavLink
                    className="navlink"
                    to="/battle"
                    onMouseOver={this.playButtonSound}
                    onMouseOut={this.stopButtonSound}>...OR BATTLE!</NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Startscreen.propTypes = {
  className: PropTypes.string
}

export default Startscreen
