import React, { useState } from 'react'
import { NavLink } from 'react-router-dom';

const Menu = () => {

  // declare hook state, and function to change state!!!
  const [showMenu, handleMenuVisibility] = useState(false)

  const visibleClass = ( showMenu ? 'open' : '' )

  return (
    <React.Fragment>
      <div className="burger-menu" onClick={() => handleMenuVisibility(!showMenu)}>
        <i className="icon-menu" />
      </div>
      <div className={`menu-choices ${visibleClass}`}>
        <i className="close-menu" onClick={() => handleMenuVisibility(!showMenu)}>×</i>
        
        <ul className="actual-menu-choices">
          <li><NavLink className="menulink" to="/">BACK TO HOME</NavLink></li>
          <li><NavLink className="menulink" to="/allheroes">CHECK ALL HEROES</NavLink></li>
          <li><NavLink className="menulink" to="/battle">...or BATTLE!</NavLink></li>
        </ul>
      </div>
    </React.Fragment>
  )
}

export default Menu