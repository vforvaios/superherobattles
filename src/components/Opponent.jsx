import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom';

const Opponent = ({ hero }) => {
  
  const { id='', name='', powerstats={}, images={} } = hero || {}
  
  return (
    <div className="col-12 hero-single-item">
      <NavLink to={`/hero/${id}`} className="hero-navlink">
        <div className="img-wrapper">
          <img src={images.lg} alt={name} />
        </div>
        <div className="lower-hero-info">
          <h4 className="hero-name mt-2 mb-3">
            {name}
          </h4>
          <div className="powerstats">
            {
              Object.keys(powerstats).map( p => {
                return (
                  <div className="powerstat" key={p}>
                    <strong>{`${p}:`}</strong>
                    <span>{powerstats[p]}</span>
                  </div>
                )
              } )
            }
          </div>
        </div>
      </NavLink>
    </div>
  )
}

Opponent.propTypes = {
  hero: PropTypes.object
}

export default Opponent