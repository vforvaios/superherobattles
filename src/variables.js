import * as epic_music from './media/epic.wav'

let bg_music = new Audio()
bg_music.src = epic_music
bg_music.loop = true

const randomizeArray = (arr) => {
  for (let i = arr.length-1; i >=0; i--) {
   
      var randomIndex = Math.floor(Math.random()*(i+1)); 
      var itemAtIndex = arr[randomIndex]; 
       
      arr[randomIndex] = arr[i]; 
      arr[i] = itemAtIndex;
  }
  return arr;
}

export { bg_music, randomizeArray }