export const GET_ALL_HEROES = 'GET_ALL_HEROES'
export const GET_SINGLE_HERO = 'GET_SINGLE_HERO'
export const LOADING = 'LOADING'